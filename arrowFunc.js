function add(...nums) {
  let total = nums.reduce((x, y) => x + y)
  console.log(`8. arrow func: ${total}`);
}
add(1, 2, 3, 4, 5)