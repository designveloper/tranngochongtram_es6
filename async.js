function abc(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(123)
    }, ms)
    
    console.log(456);
  })
}

(async function () {
  var a = await abc(1000) 
  console.log(a)
})()

console.log(789)