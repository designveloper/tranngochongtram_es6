class Animal {
  constructor(type,legs) {
    this.type = type;
    this.legs = legs;
  }
   makeNoise(sound = "Loud noises"){
    console.log(sound);
    return 123;
  }
}

let cat = new Animal('Cat', 4);
cat.makeNoise('meow')

console.log(`11. class: ${cat.makeNoise()}`)