const personInfo = {
  name: 'Joe',
  city: 'Harrisburg',
  state: 'Pennsylvania',
  zipCode: '17116'
}

const {name, state, zipCode: zc} = personInfo;

console.log(`2.Destructuring Obj: ${name}: ${state}, ${zc}`)